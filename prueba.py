'''
Created on 11 dic. 2020

@author: RSSpe
'''

from typing import List
import random
import time


class BinarySearch:
    
    def __init__(self):
        self.pasadas = 0
        self.comparaciones = 0
    
    def ejecutar(self, array, elemento):
        
        el = self.binarySearch(array, elemento, 0, len(array)-1)
        
        print(f"Pasadas: {self.pasadas}")
        print(f"comparaciones: {self.comparaciones}")
        
        self.pasadas = self.comparaciones = 0
        
        return el

    def binarySearch(self, array, buscado, primero, ultimo):
        self.pasadas+=1
        self.comparaciones+=1
        if primero>ultimo:
            return False
        else:
            mitad = (primero+ultimo)//2
            
            self.comparaciones+=1
            if buscado==array[mitad]:     
                return True
            elif buscado<array[mitad]:
                return self.binarySearch(array, buscado, primero, mitad-1)
            else:
                return self.binarySearch(array, buscado, mitad+1, ultimo)

class Hash:
    def __init__(self):
        self.table = [None] * 100
        self.comparaciones = 0
        self.pasadas = 0

    def insertar(self, value):
        hash = self.funcionHash(value)
        if self.table[hash] is None:
            self.table[hash] = value
    
    def buscarElemento(self, elemento):

        elemento = self.buscar(elemento)

        print(f"comparaciones: {self.comparaciones}")
        print(f"Pasadas: {self.pasadas}")
        
        self.pasadas = self.comparaciones = 0
        
        return elemento
    
    def funcionHash(self, value):
        key = 0
        
        for i in range(0,len(value)):
            self.comparaciones+=1
            self.pasadas+=1
            key += ord(value[i])
        return key % 100
    
    def buscar(self,value):

        hash = self.funcionHash(value)
        self.comparaciones+=1
        if self.table[hash] is None:
            return None
        else:
            return (self.table[hash])
            
    def eliminar(self,value):
        hash = self.funcionHash(value)
        if self.table[hash] is None:
            print("No hay elementos", value)
        else:
            print("Elemento con valor", value, "eliminado")
            self.table[hash] is None


def quickSort(numeros, izq, der):
        pivote = numeros[izq]
        i = izq
        j = der
        aux = 0

        while i<j:
            while numeros[i]<=pivote and i<j:
                i+=1
            while numeros[j]>pivote:
                j-=1
            if i<j:
                aux = numeros[i]
                numeros[i] = numeros[j]
                numeros[j] = aux

        numeros[izq] = numeros[j]
        numeros[j] = pivote
        if izq<j-1:
            quickSort(numeros, izq, j-1)
        if j+1<der:
            quickSort(numeros, j+1, der)
    


mb = BinarySearch()
funcion = Hash()

array = [random.randint(1, 100) for i in range(0, 100)]
quickSort(array, 0, len(array)-1)

for i in range(0,len(array)):
    funcion.insertar(str(array[i]))
    
funcion.pasadas = funcion.comparaciones = 0

print(f"Vector: {array}")

while(True):
    
    print("---------------MENU----------------")
    print("1.- Busqueda binaria")
    print("2.- Busqueda Hash")
    print("3.- Salir")
    
    try:
        opcion = int(input("Introduce una opcion: "))
    
        if opcion==1:
            
            inicio = time.time()
            
            numero = int(input("Introduce un numero: "))
            print(mb.ejecutar(array, numero))
        
            fin = time.time()
            print(f"Tiempo trancurrido: {fin-inicio}")

        elif opcion==2:
            
            inicio = time.time()
        
            numero = input("Introduce un numero: ")
            if funcion.buscarElemento(numero)!=None:
                print(True)
            else:
                print(False)
            
            fin = time.time()
            
            print(f"Tiempo transcurrido: {fin-inicio}")

        elif opcion==3:

            print("\n\n-------------------\nPrograma terminado\n-------------------")
            break

        else:
            print("Opcion invalida, prueba de nuevo")
    except ValueError as error:
        print(f"Error en la entrada de datos: {error}, vuelve a intentarlo")
    print()

